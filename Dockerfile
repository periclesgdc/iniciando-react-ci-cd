FROM node:14.15.4-alpine3.12

RUN apk add bash

USER node

RUN mkdir -p /home/node/app

WORKDIR /home/node/app

COPY . .

RUN npm install