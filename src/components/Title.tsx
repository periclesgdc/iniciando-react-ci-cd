import React from 'react';
import { Typography } from '@material-ui/core';

interface TitleProps {
  content: string;
  onClick: () => void;
}

export const Title: React.FunctionComponent<TitleProps> = (props) => {
  return (
    <Typography variant="h3" component="h1" onClick={props.onClick}>
      {props.content}
    </Typography>
  )
}