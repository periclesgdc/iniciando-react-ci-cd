import React from 'react';
import './App.css';
import { Form } from './components/Form';
import { Title } from './components/Title';
import { UsersList } from './components/UsersList';
import { Container, CssBaseline, MuiThemeProvider, Grid } from '@material-ui/core';
import theme from './theme';
import { UsersProvider } from './components/UsersProvider';

function App() {
  return (
    <UsersProvider>
      <MuiThemeProvider theme={theme}>
        <CssBaseline/>
        <Container>
          <Title content="Iniciando com React" onClick={() => alert('Teste alerta')}/>
          <Grid container>
            <Grid item xs={9}>
              <Form/>
            </Grid>
            <Grid item xs={3}>
              <UsersList/>
            </Grid>
          </Grid>
        </Container>
      </MuiThemeProvider>
    </UsersProvider>
  );
}

export default App;
